# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = '2ec229c0b5e2d0223072b1e7710fcbdc2430d2ea09ef0fe8aa5157303899684812f2afadda5f9f68be36f8a25df7008d795db11390bc4a5e2525b0a7c4480faf'
